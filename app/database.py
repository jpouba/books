from typing import List

import sqlalchemy.exc
from sqlalchemy import create_engine, select, func
from sqlalchemy.orm import Session

from db_entities import DBBook, DBUser, DBRating
from engine_entities import Book, BookWithRating, Rating, UserRating, User


class NoResultsException(Exception):
    message: str = ""

    def __init__(self, message: str):
        self.message = message


class Database:
    def __init__(self, engine_url: str) -> None:
        self.engine = create_engine(engine_url, echo=True)

    def get_books(self, limit: int = 20, offset: int = 0) -> List[BookWithRating]:
        with Session(self.engine) as session:
            stmt = select(DBBook, func.min(DBRating.rating), func.max(DBRating.rating), func.avg(DBRating.rating), func.count(DBRating.rating)) \
                .join(DBRating, isouter=True) \
                .group_by(DBBook.isbn) \
                .limit(limit).offset(offset)
            res = session.execute(stmt)
            return [_create_book_with_rating(r[0], r[1], r[2], r[3], r[4]) for r in res]

    def get_users(self, limit: int = 20, offset: int = 0) -> List[User]:
        with Session(self.engine) as session:
            stmt = select(DBUser).limit(limit).offset(offset)
            res = session.execute(stmt).scalars()
            return [_create_user(r) for r in res]

    def get_user_ratings(self, limit: int = 20, offset: int = 0) -> List[UserRating]:
        with Session(self.engine) as session:
            stmt = select(DBRating).limit(limit).offset(offset)
            res = session.execute(stmt).scalars()
            return [_create_user_rating(r) for r in res]

    def get_book(self, isbn: str) -> BookWithRating:
        with Session(self.engine) as session:
            stmt = select(DBBook, func.min(DBRating.rating), func.max(DBRating.rating), func.avg(DBRating.rating), func.count(DBRating.rating)) \
                .join(DBRating, isouter=True) \
                .group_by(DBBook.isbn) \
                .where(DBBook.isbn == isbn)
            try:
                r = session.execute(stmt).one()
            except sqlalchemy.exc.NoResultFound:
                raise NoResultsException(message=f"Book with ISBN '{isbn}' not found")
            book_with_rating: BookWithRating = _create_book_with_rating(r[0], r[1], r[2], r[3], r[4])
            return book_with_rating

    def get_user(self, user_id: int) -> User:
        with Session(self.engine) as session:
            stmt = select(DBUser).where(DBUser.id == user_id)
            try:
                res: DBUser = session.execute(stmt).scalar_one()
            except sqlalchemy.exc.NoResultFound:
                raise NoResultsException(message=f"User with ID '{user_id}' not found")
            return _create_user(res)

    def get_user_rating(self, user_id: int, isbn: str) -> UserRating:
        with Session(self.engine) as session:
            stmt = select(DBRating).where((DBRating.user_id == user_id) & (DBRating.isbn == isbn))
            try:
                res: DBRating = session.execute(stmt).scalar_one()
            except sqlalchemy.exc.NoResultFound:
                raise NoResultsException(message=f"User rating for user with ID '{user_id}' and book with ISBN '{isbn}' not found")
            return _create_user_rating(res)

    def search_books(self,
                     isbn: str | None,
                     title: str | None,
                     author: str | None,
                     year_of_publication: int | None,
                     publisher: str | None,
                     limit: int = 100,
                     offset: int = 0) -> List[BookWithRating]:
        with Session(self.engine) as session:
            stmt = select(DBBook, func.min(DBRating.rating), func.max(DBRating.rating), func.avg(DBRating.rating), func.count(DBRating.rating)) \
                .join(DBRating, isouter=True) \
                .group_by(DBBook.isbn) \
                .limit(limit).offset(offset)
            if isbn: stmt = stmt.filter(DBBook.isbn.like(f"%{isbn}%"))
            if title: stmt = stmt.filter(DBBook.title.like(f"%{title}%"))
            if author: stmt = stmt.filter(DBBook.author.like(f"%{author}%"))
            if year_of_publication: stmt = stmt.filter(DBBook.year_of_publication == year_of_publication)
            if publisher: stmt = stmt.filter(DBBook.publisher.like(f"%{publisher}%"))
            ret = session.execute(stmt)
            return [_create_book_with_rating(r[0], r[1], r[2], r[3], r[4]) for r in ret]

    def update_or_insert_book(self, book: Book) -> Book:
        with Session(self.engine) as session:
            session.merge(_create_dbbook(book))
            session.commit()
            ret_book: DBBook | None = session.scalar(select(DBBook).where(DBBook.isbn == book.isbn))
            if ret_book is None:
                raise NoResultsException(f"Error on insert/update, book with isbn {book.isbn} not found after insert/update.")
            return _create_book(ret_book)

    def update_or_insert_user(self, user: User) -> User:
        with Session(self.engine) as session:
            session.merge(_create_dbuser(user))
            session.commit()
            ret_user: DBUser | None = session.scalar(select(DBUser).where(DBUser.id == user.id))
            if ret_user is None:
                raise NoResultsException(f"Error on insert/update, user with id {user.id} not found after insert/update.")
            return _create_user(ret_user)

    def update_or_insert_user_rating(self, user_rating: UserRating) -> UserRating:
        with Session(self.engine) as session:
            session.merge(_create_dbuser_rating(user_rating))
            session.commit()
            ret_user_rating: DBRating | None = session.scalar(
                select(DBRating).where((DBRating.user_id == user_rating.user_id) & (DBRating.isbn == user_rating.book_isbn)))
            if ret_user_rating is None:
                raise NoResultsException(f"Error on insert/update, user rating with user id {user_rating.user_id} and isbn {user_rating.book_isbn} not found after insert/update.")
            return _create_user_rating(ret_user_rating)


def _create_dbbook(book: Book) -> DBBook:
    return DBBook(
        isbn=book.isbn,
        title=book.title,
        author=book.author,
        year_of_publication=book.year_of_publication,
        publisher=book.publisher,
        image_url_s=book.image_url_s,
        image_url_m=book.image_url_m,
        image_url_l=book.image_url_l
    )


def _create_dbuser(user: User) -> DBUser:
    return DBUser(
        id=user.id,
        location=user.location,
        age=user.age
    )


def _create_dbuser_rating(user_rating: UserRating) -> DBRating:
    return DBRating(
        user_id=user_rating.user_id,
        isbn=user_rating.book_isbn,
        rating=user_rating.rating
    )


def _create_user(db_user: DBUser) -> User:
    return User(id=db_user.id, location=db_user.location, age=db_user.age)


def _create_user_rating(db_rating: DBRating) -> UserRating:
    return UserRating(user_id=db_rating.user_id, rating=db_rating.rating, book_isbn=db_rating.isbn)


def _create_book_with_rating(db_book: DBBook, avg_rating: float | None, min_rating: float | None, max_rating: float | None,
                             ratings_count: int) -> BookWithRating:
    book: Book = _create_book(db_book=db_book)
    rating: Rating = Rating(
        min_rating=min_rating,
        max_rating=max_rating,
        avg_rating=avg_rating,
        ratings_count=ratings_count
    )
    return BookWithRating(book, rating)


def _create_book(db_book: DBBook) -> Book:
    return Book(
        isbn=db_book.isbn,
        title=db_book.title,
        author=db_book.author,
        year_of_publication=db_book.year_of_publication,
        publisher=db_book.publisher,
        image_url_s=db_book.image_url_s,
        image_url_m=db_book.image_url_m,
        image_url_l=db_book.image_url_l
    )
