from api_entitites import RESTBookWithRating, RESTUserRating, RESTUser, RESTBook
from engine_entities import UserRating, User, Book, BookWithRating


class RESTConverter:
    def convert_book_with_rating(self, book_with_rating: BookWithRating) -> RESTBookWithRating:
        return RESTBookWithRating(isbn=book_with_rating.book.isbn,
                                  title=book_with_rating.book.title,
                                  author=book_with_rating.book.author,
                                  year_of_publication=book_with_rating.book.year_of_publication,
                                  publisher=book_with_rating.book.publisher,
                                  image_url_s=book_with_rating.book.image_url_s,
                                  image_url_m=book_with_rating.book.image_url_m,
                                  image_url_l=book_with_rating.book.image_url_l,
                                  min_rating=book_with_rating.rating.min_rating,
                                  max_rating=book_with_rating.rating.max_rating,
                                  avg_rating=book_with_rating.rating.avg_rating,
                                  ratings_count=book_with_rating.rating.ratings_count)

    def convert_book(self, book: Book) -> RESTBook:
        return RESTBook(isbn=book.isbn,
                        title=book.title,
                        author=book.author,
                        year_of_publication=book.year_of_publication,
                        publisher=book.publisher,
                        image_url_s=book.image_url_s,
                        image_url_m=book.image_url_m,
                        image_url_l=book.image_url_l)

    def convert_user(self, user: User) -> RESTUser:
        return RESTUser(age=user.age, id=user.id, location=user.location)

    def convert_user_rating(self, user_rating: UserRating) -> RESTUserRating:
        return RESTUserRating(rating=user_rating.rating, user_id=user_rating.user_id, book_isbn=user_rating.book_isbn)

    def convert_rest_book(self, input_book: RESTBook) -> Book:
        return Book(
            isbn=input_book.isbn,
            title=input_book.title,
            author=input_book.author,
            year_of_publication=input_book.year_of_publication,
            publisher=input_book.publisher,
            image_url_s=input_book.image_url_s,
            image_url_m=input_book.image_url_m,
            image_url_l=input_book.image_url_l
        )

    def convert_rest_user(self, input_user: RESTUser) -> User:
        return User(
            location=input_user.location,
            age=input_user.age,
            id=input_user.id
        )

    def convert_rest_user_rating(self, input_user_rating: RESTUserRating) -> UserRating:
        return UserRating(
            user_id=input_user_rating.user_id,
            book_isbn=input_user_rating.book_isbn,
            rating=input_user_rating.rating
        )
