def check_and_fix_query_limit(limit: int, max_limit: int) -> int:
    if limit < 1: return 1
    if limit > max_limit: return max_limit
    return limit
