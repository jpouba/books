from dataclasses import dataclass


@dataclass
class Book:
    isbn: str
    title: str | None
    author: str | None
    year_of_publication: int | None
    publisher: str | None
    image_url_s: str | None
    image_url_m: str | None
    image_url_l: str | None


@dataclass
class Rating:
    avg_rating: float | None
    min_rating: float | None
    max_rating: float | None
    ratings_count: int


@dataclass
class BookWithRating:
    book: Book
    rating: Rating


@dataclass
class User:
    id: int
    location: str | None
    age: int | None


@dataclass
class UserRating:
    user_id: int
    book_isbn: str
    rating: int
