from typing import List
from sqlalchemy import ForeignKey
from sqlalchemy import String
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship


class Base(DeclarativeBase):
    pass


class DBUser(Base):
    __tablename__ = "BX-Users"
    id: Mapped[int] = mapped_column(name="User-ID", primary_key=True, default=0)
    location: Mapped[str | None] = mapped_column(name="Location", type_=String(250), default=None, nullable=True)
    age: Mapped[int | None] = mapped_column(name="Age", default=None, nullable=True)

    ratings: Mapped[List["DBRating"]] = relationship(back_populates="user")

    def __repr__(self) -> str:
        return f"User(id={self.id!r}, location={self.location!r}, age={self.age!r})"


class DBBook(Base):
    __tablename__ = "BX-Books"
    isbn: Mapped[str] = mapped_column(name="ISBN", primary_key=True, type_=String(13), default="")
    title: Mapped[str | None] = mapped_column(name="Book-Title", type_=String(255), default=None, nullable=True)
    author: Mapped[str | None] = mapped_column(name="Book-Author", type_=String(255), default=None, nullable=True)
    year_of_publication: Mapped[int | None] = mapped_column(name="Year-Of-Publication", default=None, nullable=True)
    publisher: Mapped[str | None] = mapped_column(name="Publisher", type_=String(255), default=None, nullable=True)
    image_url_s: Mapped[str | None] = mapped_column(name="Image-URL-S", type_=String(255), default=None, nullable=True)
    image_url_m: Mapped[str | None] = mapped_column(name="Image-URL-M", type_=String(255), default=None, nullable=True)
    image_url_l: Mapped[str | None] = mapped_column(name="Image-URL-L", type_=String(255), default=None, nullable=True)

    ratings: Mapped[List["DBRating"]] = relationship(back_populates="book")

    def __repr__(self) -> str:
        return f"Book(isbn={self.isbn!r}, title={self.title!r}, author={self.author!r})"


class DBRating(Base):
    __tablename__ = "BX-Book-Ratings"
    user_id: Mapped[int] = mapped_column(ForeignKey("BX-Users.User-ID"), primary_key=True)
    isbn: Mapped[str] = mapped_column(ForeignKey("BX-Books.ISBN"), primary_key=True)
    rating: Mapped[int] = mapped_column(name="Book-Rating", nullable=False)

    user: Mapped[DBUser] = relationship(back_populates="ratings")
    book: Mapped[DBBook] = relationship(back_populates="ratings")

    def __repr__(self) -> str:
        return f"Rating(user={self.user_id!r}, book={self.isbn!r}, rating={self.rating!r})"
