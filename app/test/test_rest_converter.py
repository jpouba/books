from app.rest_converter import RESTConverter
from app.engine_entities import BookWithRating, Rating, Book, UserRating, User
from app.api_entitites import RESTBookWithRating, RESTUser, RESTUserRating, RESTBook


def test_convert_book_with_rating() -> None:
    rc: RESTConverter = RESTConverter()
    book: Book = Book(
        isbn="isbn",
        title="title",
        author="author",
        year_of_publication=25,
        publisher="publisher",
        image_url_s="image_url_s",
        image_url_m="image_url_m",
        image_url_l="image_url_l",
    )
    rating: Rating = Rating(
        min_rating=0.1,
        max_rating=0.2,
        avg_rating=0.3,
        ratings_count=38
    )
    book_with_rating: BookWithRating = BookWithRating(book, rating)
    rest_book_with_rating: RESTBookWithRating = rc.convert_book_with_rating(book_with_rating)
    assert rest_book_with_rating.isbn == "isbn"
    assert rest_book_with_rating.title == "title"
    assert rest_book_with_rating.author == "author"
    assert rest_book_with_rating.year_of_publication == 25
    assert rest_book_with_rating.publisher == "publisher"
    assert rest_book_with_rating.image_url_s == "image_url_s"
    assert rest_book_with_rating.image_url_m == "image_url_m"
    assert rest_book_with_rating.image_url_l == "image_url_l"
    assert rest_book_with_rating.min_rating == 0.1
    assert rest_book_with_rating.max_rating == 0.2
    assert rest_book_with_rating.avg_rating == 0.3
    assert rest_book_with_rating.ratings_count == 38


def test_convert_book() -> None:
    rc: RESTConverter = RESTConverter()
    book: Book = Book(
        isbn="isbn",
        title="title",
        author="author",
        year_of_publication=25,
        publisher="publisher",
        image_url_s="image_url_s",
        image_url_m="image_url_m",
        image_url_l="image_url_l",
    )
    rest_book: RESTBook = rc.convert_book(book)
    assert rest_book.isbn == "isbn"
    assert rest_book.title == "title"
    assert rest_book.author == "author"
    assert rest_book.year_of_publication == 25
    assert rest_book.publisher == "publisher"
    assert rest_book.image_url_s == "image_url_s"
    assert rest_book.image_url_m == "image_url_m"
    assert rest_book.image_url_l == "image_url_l"


def test_convert_user() -> None:
    rc: RESTConverter = RESTConverter()
    user: User = User(id=0, location="location", age=150)
    rest_user: RESTUser = rc.convert_user(user)
    assert rest_user.id == 0
    assert rest_user.location == "location"
    assert rest_user.age == 150


def test_convert_user_rating() -> None:
    rc: RESTConverter = RESTConverter()
    user_rating: UserRating = UserRating(user_id=10, book_isbn="book_isbn", rating=8)
    rest_user_rating: RESTUserRating = rc.convert_user_rating(user_rating)
    assert rest_user_rating.user_id == 10
    assert rest_user_rating.book_isbn == "book_isbn"
    assert rest_user_rating.rating == 8


def test_convert_rest_book() -> None:
    rc: RESTConverter = RESTConverter()
    rest_book: RESTBook = RESTBook(
        isbn="isbn",
        title="title",
        author="author",
        year_of_publication=25,
        publisher="publisher",
        image_url_s="image_url_s",
        image_url_m="image_url_m",
        image_url_l="image_url_l",
    )
    book: Book = rc.convert_rest_book(rest_book)
    assert book.isbn == "isbn"
    assert book.title == "title"
    assert book.author == "author"
    assert book.year_of_publication == 25
    assert book.publisher == "publisher"
    assert book.image_url_s == "image_url_s"
    assert book.image_url_m == "image_url_m"
    assert book.image_url_l == "image_url_l"


def test_convert_rest_user() -> None:
    rc: RESTConverter = RESTConverter()
    rest_user: RESTUser = RESTUser(id=5, location="LOC", age=38)
    user: User = rc.convert_rest_user(rest_user)
    assert user.id == 5
    assert user.location == "LOC"
    assert user.age == 38


def test_convert_rest_user_rating() -> None:
    rc: RESTConverter = RESTConverter()
    rest_user_rating: RESTUserRating = RESTUserRating(user_id=11, book_isbn="isbn", rating=5)
    user_rating: UserRating = rc.convert_rest_user_rating(rest_user_rating)
    assert user_rating.user_id == 11
    assert user_rating.book_isbn == "isbn"
    assert user_rating.rating == 5
