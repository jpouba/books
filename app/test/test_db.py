from app.database import _create_user, _create_user_rating, _create_book, _create_dbuser, _create_dbbook, _create_dbuser_rating, _create_book_with_rating
from app.db_entities import DBBook, DBUser, DBRating
from app.engine_entities import Book, BookWithRating, User, UserRating, Rating


def test_create_user() -> None:
    db_user: DBUser = DBUser(id=0, location="location", age=20)
    user = _create_user(db_user)
    assert user.id == 0
    assert user.location == "location"
    assert user.age == 20


def test_create_user_rating() -> None:
    db_user_rating: DBRating = DBRating(user_id=0, isbn="isbn", rating=5)
    user_rating: UserRating = _create_user_rating(db_user_rating)
    assert user_rating.rating == 5
    assert user_rating.book_isbn == "isbn"
    assert user_rating.user_id == 0


def test_create_book() -> None:
    db_book: DBBook = DBBook(
        isbn="isbn",
        title="title",
        author="author",
        year_of_publication=0,
        publisher="publisher",
        image_url_s="small",
        image_url_m="medium",
        image_url_l="large"
    )
    book: Book = _create_book(db_book)
    assert book.isbn == "isbn"
    assert book.title == "title"
    assert book.author == "author"
    assert book.year_of_publication == 0
    assert book.publisher == "publisher"
    assert book.image_url_s == "small"
    assert book.image_url_m == "medium"
    assert book.image_url_l == "large"


def test_create_book_with_rating() -> None:
    db_book: DBBook = DBBook(
        isbn="isbn",
        title="title",
        author="author",
        year_of_publication=0,
        publisher="publisher",
        image_url_s="small",
        image_url_m="medium",
        image_url_l="large"
    )
    book_with_rating: BookWithRating = _create_book_with_rating(db_book, 0.1, 0.2, 0.3, 50)
    assert book_with_rating.book.isbn == "isbn"
    assert book_with_rating.book.title == "title"
    assert book_with_rating.book.author == "author"
    assert book_with_rating.book.year_of_publication == 0
    assert book_with_rating.book.publisher == "publisher"
    assert book_with_rating.book.image_url_s == "small"
    assert book_with_rating.book.image_url_m == "medium"
    assert book_with_rating.book.image_url_l == "large"

    assert book_with_rating.rating.avg_rating == 0.1
    assert book_with_rating.rating.min_rating == 0.2
    assert book_with_rating.rating.max_rating == 0.3
    assert book_with_rating.rating.ratings_count == 50


def test_create_dbuser() -> None:
    user: User = User(id=10, location="loc", age=25)
    db_user: DBUser = _create_dbuser(user)
    assert db_user.id == 10
    assert db_user.location == "loc"
    assert db_user.age == 25


def test_create_dbuser_rating() -> None:
    user_rating: UserRating = UserRating(user_id=1, book_isbn="isbn", rating=5)
    db_rating: DBRating = _create_dbuser_rating(user_rating)
    assert db_rating.rating == 5
    assert db_rating.isbn == "isbn"
    assert db_rating.user_id == 1


def test_create_dbbook() -> None:
    book: Book = Book(
        isbn="isbn",
        title="title",
        author="author",
        year_of_publication=0,
        publisher="publisher",
        image_url_s="small",
        image_url_m="medium",
        image_url_l="large"
    )
    db_book: DBBook = _create_dbbook(book)
    assert db_book.isbn == "isbn"
    assert db_book.title == "title"
    assert db_book.author == "author"
    assert db_book.year_of_publication == 0
    assert db_book.publisher == "publisher"
    assert db_book.image_url_s == "small"
    assert db_book.image_url_m == "medium"
    assert db_book.image_url_l == "large"
