from rest_util import check_and_fix_query_limit


def test_check_and_fix_query_limit() -> None:
    assert check_and_fix_query_limit(0, max_limit=50) == 1
    assert check_and_fix_query_limit(1, max_limit=50) == 1
    assert check_and_fix_query_limit(10, max_limit=200) == 10
    assert check_and_fix_query_limit(200, max_limit=200) == 200
    assert check_and_fix_query_limit(201, max_limit=200) == 200
    assert check_and_fix_query_limit(10000, max_limit=200) == 200
    assert check_and_fix_query_limit(10000, max_limit=300) == 300
    assert check_and_fix_query_limit(-1, max_limit=200) == 1
    assert check_and_fix_query_limit(-10, max_limit=200) == 1
