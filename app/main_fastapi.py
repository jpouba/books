from fastapi import FastAPI, HTTPException
from typing import List

import config
from rest_converter import RESTConverter
from database import Database, NoResultsException
from engine_entities import BookWithRating, User, UserRating
from api_entitites import RESTBookWithRating, RESTUser, RESTUserRating, RESTBook, RESTMessage
from rest_util import check_and_fix_query_limit

app = FastAPI()
database: Database = Database(config.DB_ENGINE_URL)
converter = RESTConverter()


@app.get("/")
def root() -> RESTMessage:
    return RESTMessage(message="Hello World!")


@app.get("/books")
def books(limit: int = 100, offset: int = 0) -> List[RESTBookWithRating]:
    db_books_with_rating: List[BookWithRating] = database.get_books(limit=_check_and_fix_query_limit(limit), offset=offset)
    return [converter.convert_book_with_rating(db_book) for db_book in db_books_with_rating]


@app.get("/users")
def users(limit: int = 100, offset: int = 0) -> List[RESTUser]:
    db_users: List[User] = database.get_users(limit=_check_and_fix_query_limit(limit), offset=offset)
    return [converter.convert_user(db_user) for db_user in db_users]


@app.get("/userratings")
def user_ratings(limit: int = 100, offset: int = 0) -> List[RESTUserRating]:
    db_user_ratings: List[UserRating] = database.get_user_ratings(limit=_check_and_fix_query_limit(limit), offset=offset)
    return [converter.convert_user_rating(db_user_rating) for db_user_rating in db_user_ratings]


@app.get("/book/{isbn}")
def book(isbn: str) -> RESTBookWithRating:
    try:
        db_book: BookWithRating = database.get_book(isbn)
    except NoResultsException as e:
        raise HTTPException(status_code=404, detail=e.message)
    return converter.convert_book_with_rating(db_book)


@app.get("/user/{user_id}")
def user(user_id: int) -> RESTUser:
    try:
        db_user: User = database.get_user(user_id=user_id)
    except NoResultsException as e:
        raise HTTPException(status_code=404, detail=e.message)
    return converter.convert_user(db_user)


@app.get("/userrating/{user_id}/{book_isbn}")
def userrating(user_id: int, book_isbn: str) -> RESTUserRating:
    try:
        db_user_rating: UserRating = database.get_user_rating(user_id=user_id, isbn=book_isbn)
    except NoResultsException as e:
        raise HTTPException(status_code=404, detail=e.message)
    return converter.convert_user_rating(db_user_rating)


@app.get("/search/books")
def search(isbn: str | None = None,
         title: str | None = None,
         author: str | None = None,
         year_of_publication: int | None = None,
         publisher: str | None = None,
         limit: int = 100,
         offset: int = 0) -> List[RESTBookWithRating]:
    books: List[BookWithRating] = database.search_books(isbn, title, author, year_of_publication, publisher, _check_and_fix_query_limit(limit), offset)
    return [converter.convert_book_with_rating(b) for b in books]


@app.put("/book")
def new_book(book: RESTBook) -> RESTBook:
    ret = database.update_or_insert_book(converter.convert_rest_book(book))
    return converter.convert_book(ret)


@app.put("/user")
def new_user(user: RESTUser) -> RESTUser:
    ret = database.update_or_insert_user(converter.convert_rest_user(user))
    return converter.convert_user(ret)


@app.put("/userrating")
def new_user_rating(user_rating: RESTUserRating) -> RESTUserRating:
    ret = database.update_or_insert_user_rating(converter.convert_rest_user_rating(user_rating))
    return converter.convert_user_rating(ret)


def _check_and_fix_query_limit(limit: int) -> int:
    ret: int = check_and_fix_query_limit(limit=limit, max_limit=config.MAX_QUERY_LIMIT)
    return ret
