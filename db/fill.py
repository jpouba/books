from sqlalchemy import create_engine, text

engine = create_engine("<placeholder>", echo=False)


def main() -> None:
    with engine.connect() as conn:
        for filename in ["BX-SQL-Dump/BX-Books.sql", "BX-SQL-Dump/BX-Users.sql", "BX-SQL-Dump/BX-Book-Ratings.sql"]:
            with open(filename) as file:
                for line in file:
                    string = line.rstrip().replace("\\\'", "''").replace(":", "\\:")
                    stmt = text(string)
                    conn.execute(stmt)
        conn.commit()


if __name__ == "__main__":
    main()
