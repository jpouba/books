"""Limit strings in book table

Revision ID: 41a815cb1e53
Revises: 0f1086ce9bc1
Create Date: 2023-02-01 12:32:10.030258

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '41a815cb1e53'
down_revision = '0f1086ce9bc1'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("ALTER TABLE \"BX-Books\" ALTER COLUMN \"Book-Title\" TYPE VARCHAR(255) ")
    op.execute("ALTER TABLE \"BX-Books\" ALTER COLUMN \"Book-Author\" TYPE VARCHAR(255) ")


def downgrade() -> None:
    op.execute("ALTER TABLE \"BX-Books\" ALTER COLUMN \"Book-Title\" TYPE VARCHAR")
    op.execute("ALTER TABLE \"BX-Books\" ALTER COLUMN \"Book-Author\" TYPE VARCHAR")
